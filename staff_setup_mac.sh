#! /usr/bin/env bash

set -e

kubectl_version=${KUBECTL_VERSION:-'v1.27.3'}
argo_version=${ARGO_VERSION:-'v2.0.3'}

# run community setup
./community_setup_mac.sh

# azure
brew update && brew install azure-cli
az login

#kubectl
curl -LO https://storage.googleapis.com/kubernetes-release/release/"$kubectl_version"/bin/darwin/amd64/kubectl
chmod +x ./kubectl
sudo mv ./kubectl /usr/local/bin/kubectl

# set up cluster connection 
az account set -s Primary
az aks get-credentials -g grand-central-station-east-1ac573a3 -n terminal2487a2f --admin

# linkerd
brew install linkerd

# argo
curl -sLO https://github.com/argoproj/argo-cd/releases/download/"$argo_version"/argocd-darwin-amd64
chmod +x argocd-darwin-amd64
mv ./argocd-darwin-amd64 /usr/local/bin/argocd

# pulumi
brew install pulumi

# jq
brew install jq

# doppler
brew install dopplerhq/cli/doppler

# helm
brew install helm

# For running ansible tools in grand-central-station
brew install python3
python3 -m pip install --upgrade pip
pip3 install virtualenv
brew install --cask 1password/tap/1password-cli

# telepresence
echo "Install telepresence for correct architecture https://www.telepresence.io/docs/latest/install/"
