#! /usr/bin/env bash

set -e

if ! command -v docker &> /dev/null
then
    echo "docker must be installed and executable to continue"
    exit
fi

node_version=${NODE_VERSION:-'v20.10.0'}

# buildpacks
brew install buildpacks/tap/pack

# postgres
docker pull postgres:14

# psql
brew install postgresql

# redis and cli
docker pull redis:6.2.3
brew install redis

# dmbate
brew install dbmate

# detect-secrets
brew install detect-secrets

#nvm 
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | $SHELL
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"
nvm install "$node_version"
