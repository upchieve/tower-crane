#! /usr/bin/env bash

set -e

if ! command -v docker &> /dev/null
then
    echo "docker must be installed and executable to continue"
    exit 1
fi

# version variables
pack_version=${PACK_VERSION:-'v0.27.0'}
docker_compose_version=${DOCKER_COMPOSE_VERSION:-'2.9.0'}
node_version=${NODE_VERSION:-'v20.10.0'}

# buildpacks
(curl -sSL "https://github.com/buildpacks/pack/releases/download/$pack_version/pack-$pack_version-linux.tgz" | sudo tar -C /usr/local/bin/ --no-same-owner -xzv pack)

# docker-compose
sudo curl -L "https://github.com/docker/compose/releases/download/$docker_compose_version/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

# redis
docker pull redis:6.2.3

# dbmate
sudo curl -fsSL -o /usr/local/bin/dbmate https://github.com/amacneil/dbmate/releases/latest/download/dbmate-linux-amd64
sudo chmod +x /usr/local/bin/dbmate

# detect-secrets
pip install detect-secrets

# nvm
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.37.2/install.sh | bash

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"

nvm install "$node_version"
