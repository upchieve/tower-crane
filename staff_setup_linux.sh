#! /usr/bin/env bash

set -e

if ! command -v docker &> /dev/null
then
    echo "docker must be installed and executable to continue"
    exit 1
fi

# version variables
kubectl_version=${KUBECTL_VERSION:-'v1.27.3'}
argo_version=${ARGO_VERSION:-'v2.0.3'}

./community_setup_linux.sh
sudo apt install -y curl

# azure
curl -L https://aka.ms/InstallAzureCli | $SHELL
az login

# kubectl
curl -LO "https://dl.k8s.io/release/$kubectl_version/bin/linux/amd64/kubectl"
sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
rm kubectl

# set up cluster connection 
az account set -s 0c13b281-e8f9-4061-912f-6e27c8ae9073
az aks get-credentials -g grand-central-station-east-1ac573a3 -n terminal2487a2f --admin

# linkerd
curl -sL https://run.linkerd.io/install | $SHELL

# argo cli
curl -sSL -o /usr/local/bin/argocd https://github.com/argoproj/argo-cd/releases/download/$argo_version/argocd-linux-amd64
chmod +x /usr/local/bin/argocd

# pulumi
curl -fsSL https://get.pulumi.com | $SHELL

# doppler
sudo apt-get update && sudo apt-get install -y apt-transport-https ca-certificates curl gnupg
curl -sLf --retry 3 --tlsv1.2 --proto "=https" 'https://packages.doppler.com/public/cli/gpg.DE2A7741A397C129.key' | sudo apt-key add -
echo "deb https://packages.doppler.com/public/cli/deb/debian any-version main" | sudo tee /etc/apt/sources.list.d/doppler-cli.list
sudo apt-get update && sudo apt-get install doppler

# helm
curl https://baltocdn.com/helm/signing.asc | gpg --dearmor | sudo tee /usr/share/keyrings/helm.gpg > /dev/null
sudo apt-get install apt-transport-https --yes
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/helm.gpg] https://baltocdn.com/helm/stable/debian/ all main" | sudo tee /etc/apt/sources.list.d/helm-stable-debian.list
sudo apt-get update
sudo apt-get install helm

# jq
sudo apt-get install jq

# For running ansible tools in grand-central-station
sudo apt install -y python3-pip
python3 -m pip install --upgrade pip
pip3 install virtualenv

# 1Password CLI
curl -sS https://downloads.1password.com/linux/keys/1password.asc | \
  sudo gpg --dearmor --output /usr/share/keyrings/1password-archive-keyring.gpg
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/1password-archive-keyring.gpg] https://downloads.1password.com/linux/debian/$(dpkg --print-architecture) stable main" | \
  sudo tee /etc/apt/sources.list.d/1password.list
sudo mkdir -p /etc/debsig/policies/AC2D62742012EA22/
curl -sS https://downloads.1password.com/linux/debian/debsig/1password.pol | \
 sudo tee /etc/debsig/policies/AC2D62742012EA22/1password.pol
sudo mkdir -p /usr/share/debsig/keyrings/AC2D62742012EA22
curl -sS https://downloads.1password.com/linux/keys/1password.asc | \
 sudo gpg --dearmor --output /usr/share/debsig/keyrings/AC2D62742012EA22/debsig.gpg
sudo apt update && sudo apt install 1password-cli

# telepresence
sudo curl -fL https://app.getambassador.io/download/tel2/linux/amd64/latest/telepresence -o /usr/local/bin/telepresence
sudo chmod a+x /usr/local/bin/telepresence
